package pieces;

public abstract class Piece{
    private char file;
    private int rank;
    private Color color;
    public void setFile(char file) throws IndexOutOfBoundException{
        if(file < 'a' || file > 'h')
            throw new IndexOutOfBoundException(file +" is not a valid file coordinate.")
        this.file = file;
    }
    public void setRank(int rank) throws IndexOutOfBoundException{
        if(rank < 1 || rank > 8)
            throw new IndexOutOfBoundException(rank + " is not a valid rank coordinate.");
        this.rank = rank; 
    }
    public void setColor(Color color){
        this.color = color;
    }
}