package game;
 
/** A Player interface
    @author Pierre 
*/
interface Player{
    void play (Move move, Game game;)
}
/**A (chess) Game interface
    @author Pierre 
*/
interface Game {
    void setMove(Exec cmd, String loc, String dest);
    void exec();
}

enum Exec {MOVE, PROMOTE, RESIGN, DRAW}

/** Class describing a move in a game
    @author Pierre
 */
class Move{
    private static Move move = null;
    private Exec cmd;
    private String loc;
    private String dest;
    private String command;

    /** Construct a move
    * @param command the move 
    */
    private Move(String command) throws IllegalArgumentException{
        this.update(command);
        this.command = command;
    }

    private void update(String command) throws IllegalArgumentException{
        command = command.trim();
        if(command.matches("[a-hA-H][1-8]\\s[a-hA-H][1-8]")){
            cmd = Exec.BASIC;
            loc = command.substring(0,2);
            dest = command.substring(3,command.length());
        }else if(command.matches("[a-hA-H][1-8]\\s[a-hA-H][1-8]\\s")){
            cmd = Exec.PROMOTE;
            loc = command.substring(0,2);
            dest = command.substring(3, command.indexOf(" ", 3));
        }else if(command.matches("[a-hA-H][1-8]\\s[a-hA-H][1-8]\\s\\(draw?\\)"))

        this.command = command;
    }

    /** returns an instance of Move
        @param command a string describing a move
        @return a Move instance
        @throws IllegalArgumentException if command is not a recognized format
     */
    static Move getInstance(String command) throws IllegalArgumentException{
        if (move == null){
            move = New Move(command);
        }
        move.update(command);
    }

    /** returns a command/operation
        @return name of command
     */
    Exec getCMD{
        return cmd;
    }

    /** Returns initial location
        @return the location of a piece player wants to move
    */
    String getLoc(){
        return loc;
    }

    /** returns desired destination
        @return the location a piece is to move to
     */
     String getDest(){
         return dest;
     }

    /** return command
        @return command 
    */
    String getCommand(){
        return command;
    }

     @override
     String toString(){
         return command;
     }
}