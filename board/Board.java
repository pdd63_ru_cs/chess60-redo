package board;
import game.*;
import pieces.*;
public class Board{
    private class Square{
        Color color;
        Piece pc;
        char file;
        int rank;
        Square(Color color, char file, int rank){
            this.color = color;
            this.file = file;
            this.rank = rank;
        }
    }
    private Piece [][] board = new Piece[8][8];
    private ArrayList<Piece> white_pcs;
    private ArrayList<Piece> black_pcs;
    public boolean 
    public Board(){
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(((i % 2 == 0 )&& (j % 2 == 0)) || ((i % 2 != 0) && (j % 2 !=0))
                board[i][j]   
            }
        }
    }
    public void setBoard(){
        for(char file = 'a'; file <= 'h' ; file++ ){
            //set all white pieces on rank 2
            setPiece(new Pawn(Color.WHITE, file, 2), file , 2);
            //set all black pieces on rank 7
            setPiece(new Pawn(Color.BLACK, file, 7), file, 7);
        }
        // set Rook pieces
        setPiece(new Rook(Color.WHITE, 'a', 1), 'a', 1);
        setPiece(new Rook(Color.WHITE, 'h', 1), 'h', 1);
        setPiece(new Rook(Color.BLACK, 'a', 8), 'a', 8);
        setPiece(new Rook(Color.BLACK, 'h', 8), 'h', 8);

        // set Knight pieces
        setPiece(new Knight(Color.WHITE, 'b', 1), 'b', 1);
        setPiece(new Knight(Color.WHITE, 'g', 1), 'g', 1);
        setPiece(new Knight(Color.BLACK, 'b', 8), 'b', 8);
        setPiece(new Knight(Color.BLACK, 'g', 8), 'g', 8);

        // set Bishop pieces
        setPiece(new Bishop(Color.WHITE, 'c', 1), 'c', 1);
        setPiece(new Bishop(Color.WHITE, 'f', 1), 'f', 1);
        setPiece(new Bishop(Color.BLACK, 'c', 8), 'c', 8);
        setPiece(new Bishop(Color.BLACK, 'f', 8), 'f', 8);

        // Set Queen pieces
        setPiece(new Queen(Color.WHITE, 'd', 1), 'd', 1);
        setPiece(new Queen(Color.BLACK, 'd', 8), 'd', 8);

        // Set King pieces
        setPiece(new King(Color.WHITE, 'e', 1), 'e', 1);
        setPiece(new King(Color.BLACK, 'e', 8), 'e', 8);

    }
    public void removePiece(char file, int rank) throws IndexOutOfBoundException{
        if(rank > 8 || rank < 0 || file < 'a' || file > 'h')
            throw new IndexOutOfBoundException(file +"" +rank +" is not valid.");

        board[rank-1][file - 'a'] = null;
    }
    public Piece getPiece(char file, int rank){
        if(rank > 8 || rank < 0 || file < 'a' || file > 'h')
            throw new IndexOutOfBoundException(file +"" +rank +" is not valid.");
        
        return board[rank-1][file - 'a'];
    }
    public void setPiece(Piece piece, char file, int rank) throws IndexOutOfBoundException{
        if(rank > 8 || rank < 0 || file < 'a' || file > 'h')
            throw new IndexOutOfBoundException(file +"" +rank +" is not valid.");  
       
        board[rank-1][file - 'a'] = piece;
        piece.setFile(file);
        piece.setRank(rank);    
    }

    public void display(){
        String dsp;
        Piece pc;
        for (int rank = 1; rank <= 8; rank++){
           for (char file = 'a'; file <= 'h'; file++){
               dsp = "";
               if ((pc = getPiece(file, rank)) != null){
                   //copy abrev to dsp
               }
           }
        }
    }
}